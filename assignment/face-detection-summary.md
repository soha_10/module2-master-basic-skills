# **FACE RECOGNITION**

<div align="center"><img src="https://clearview-communications.com/wp-content/uploads/2020/04/Facial-Recognition-B-2.jpg"  width="1000" height="300"></div>

Facial recognition is a category of biometric software that maps an individual's facial features mathematically and stores the data as a faceprint. The software uses deep learning algorithms to compare a live capture or digital image to the stored faceprint in order to verify an individual's identity. 

> ***Steps involved are**:*

<div align="center"><img src="https://miro.medium.com/max/4658/1*sgROTW1Wa-u4hhUN62tWyw.png"  width="800" height="300"></div>

## *Examples of facial recognition*


 | **Application** | **Airports Secuirity** |
| ------ | ------ |
|Capture & Stores information of an indiividual for afey and secuirity purposes.| <img src="https://www.reservations.com/blog/wp-content/uploads/2019/05/02-facial-recognition-airports-reservations-how-works.png"  width="600" height="300"> |

| **Application** | **Face2Gene** |
| ------ | ------ |
|The app helps medical practitioners compare genetic and phenotypic traits to identify different syndromes within individuals.| <img src="https://www.cubix.co/storage/app/media/blog-images/face2gene.jpg"  width="950" height="300"> |

### Benefits of Facial Recognition:
 - Faster processing

 - Seamless integration

 - Automation of identification

### Threats of Facial Recognition:
 - Breach of privacy

 -  Vulnerability in recognition

 - Massive data storage

<div align="center"><img src="https://deeprootsathome.com/wp-content/uploads/2020/07/Is-Lockdown-2.0-a-Beta-Test-for-AI-Facial-Recognition.jpg"  width="800" height="300"></div>

> *Thereby, it has much potential in almost all areas of life, including medical, business, and law enforcement if used wisely.*
